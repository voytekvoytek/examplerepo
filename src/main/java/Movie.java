package main.java;

import java.time.LocalDateTime;

/**
 * Created by RENT on 2017-08-16.
 */
public class Movie {
    private String nazwaFilmu;
    private String nazwiskoAutora;
    private LocalDateTime data;
    private MovieType typ;

    public Movie(String nazwaFilmu, String nazwiskoAutora, LocalDateTime data, MovieType typ) {
        this.nazwaFilmu = nazwaFilmu;
        this.nazwiskoAutora = nazwiskoAutora;
        this.data = data;
        this.typ = typ;
    }

    public String getNazwaFilmu() {
        return nazwaFilmu;
    }

    public void setNazwaFilmu(String nazwaFilmu) {
        this.nazwaFilmu = nazwaFilmu;
    }

    public String getNazwiskoAutora() {
        return nazwiskoAutora;
    }

    public void setNazwiskoAutora(String nazwiskoAutora) {
        this.nazwiskoAutora = nazwiskoAutora;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public MovieType getTyp() {
        return typ;
    }

    public void setTyp(MovieType typ) {
        this.typ = typ;
    }
}
